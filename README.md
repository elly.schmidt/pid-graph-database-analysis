# PID graph database analysis

This repository contains some files and data I created in the course of my Bachelor's thesis with the topic "Analysis of a large database of persistent identifiers using graph technologies".

The [statistics](statistics) folder contains the gathered data, that is statistics of properties of the examined Neo4j database that contains the pid data.
The corresponding ``matplotlib`` plots can be found in the [plots](plots) directory.

Check the Jupyter notebooks to see what queries were used to collect the statistics and how the plots were created.


For future analysis the [notebook templates](notebook_templates) can be used.

### Requirements
This project requires some third-party packages.
Install the required packages by running the following command:

```["shell"]
pip install -r requirements.txt`
```

## Documentation

This project is documented using the docstring format `numpydoc`.
For generating documentation we use [pdoc3](https://pdoc3.github.io/pdoc/doc/pdoc/) (which can be installed with `pip install pdoc3`).  

To generate documentation, use the following command:

```["shell"]
pdoc --html --force --output-dir doc src
```

This will create a `doc`-directory, containing the documentation.
You can view the documentation by opening the `index.html`-file inside the `doc/src`-directory in your browser. 

We set the `--force`-flag, in order to overwrite old documentation inside the `doc`-directory.  
We set the `--output-dir` to `doc`, in order to put the documentation inside the `doc`-directory (instead of the default `html`-directory).


### Links
* [Neo4j Documentation](https://neo4j.com/)
* [ePIC PID Service](https://pidconsortium.net)
* [Graph Database](https://graphdb.gwdg.de/browser/)
