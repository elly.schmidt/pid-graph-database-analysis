"""This file contains some useful functions to operate with a Neo4j database.
"""

from py2neo import Graph
import getpass
from src.config import DB_URL, DB_NAME
from neo4j import GraphDatabase
import pandas as pd

def connect_to_database_py2neo(db_url=DB_URL):
    """Connects to a Neo4j database and returns the contained graph using py2neo.

    This function does not support the feature to select a database out of
    multiple databases which is a feature in Neo4j 4.0.x.

    The url of the databasd is specified by `DB_URL`. Calling this function
    opens a prompt for inserting username and password.

    Parameters
    ----------
    db_url: str
        The URL of the database we want to operate on. By default this is the
        url of our pid database.

    Returns
    -------
    py2neo.Graph
        The graph contained in the database.

    """
    graph = Graph(DB_URL, auth=(getpass.getpass(prompt='User: '), getpass.getpass(prompt='Password: ')))

    return graph



def connect_to_database(db_url=DB_URL, db_name=DB_NAME):
    """Connects to a Neo4j database and returns the driver and its session

    The url of the databasd is specified by `DB_URL`. Calling this function
    opens a prompt for inserting username and password.

    Parameters
    ----------
    db_url: str
        The URL of the database we want to operate on. By default this is the
        url of our pid database.
    db_name: str
        The name of the database we want to select. By default this is the name
        of the pid database

    Returns
    -------
    neo4j.Driver
        The driver that connects to the database

    neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session
    """
    driver = GraphDatabase.driver(DB_URL, auth=(getpass.getpass(prompt='User: '), getpass.getpass(prompt='Password: ')))
    mydb = driver.session(database=DB_NAME)
    return driver, mydb



def run_query_py2neo(graph: Graph, query: str):
    """Runs the specified query on the Neo4j database and returns the result.

    This works well for simple queries returning a single value, e.g. an int.
    For queries returning a dataframe, use the method `run_query_dataframe`.

    Parameters
    ----------
    graph: py2neo.Graph
        the graph on which the query should be run
    query: str
        a Cypher query

    Returns
    -------
    some type
        The evaluation of the given query. This can be e.g. an int or a list
        depending on the query.

    """

    return graph.evaluate(query)


def run_query_py2neo_dataframe(graph: Graph, query: str):
    """Runs the specified query on the Neo4j database and returns the result.

    This works for queries returning a dataframe.

    Parameters
    ----------
    graph: py2neo.Graph
        the graph on which the query should be run
    query: str
        the query to be executed

    Returns
    -------
    dataframe
        the evaluation of the given query.

    """
    df = graph.run(query).to_data_frame()
    return df

def run_query(session, query:str, read=True, suppress_output=False):
    """Runs cypher query on database and prints result.

    Parameters
    ----------
    session: neo4j.Session
        The current session object of the used to driver
    query: str
        a Cypher query
    read: bool
        if true the given query is executed as a read_transaction, otherwise as
        a write_transaction
    suppress_output: bool
        if true, the default output of the result will be suppressed. 
        Per default this is set false.

    Returns
    -------
    pandas dataframe
        The result of the executed query
    """

    def execute(tx, cypher):
        if not suppress_output:
            print("Executing query \'{}\'...".format(cypher))
        data = tx.run(cypher)
        res = pd.DataFrame([r.values() for r in data], columns=data.keys())
        return res

    if read:
        result = session.read_transaction(execute, query)
    else:
        result = session.write_transaction(execute, query)

    if not suppress_output:
        print("\n-------------------------------------\n-------------- Result: --------------\n-------------------------------------\n")
        print(result)
        
    return result



