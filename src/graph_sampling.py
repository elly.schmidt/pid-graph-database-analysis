"""This module contains methods to create graph samples of a Neo4j
graph database and some functionality to add attributes for Gephi plots.
"""

from src.gdb_utils import connect_to_database, run_query
import src.file_management as file_management

import networkx as nx # python graph object
import random # for random node and edge selection
import numpy as np

# Color attributes for Gephi in correspondence to node labels:
METADATA_COLOR = {'r': 242, 'g': 223, 'b': 182, 'a': 1} # beige
URL_COLOR = {'r': 141, 'g': 204, 'b': 147, 'a': 1} # green (from Neo4j, inner node color)
HANDLE_COLOR = {'r': 241, 'g': 102, 'b': 103, 'a': 1} # red (from Neo4j, inner node color)
#URL_COLOR = {'r': 93, 'g': 182, 'b': 101, 'a': 1} # green (from Neo4j, node border color)
#HANDLE_COLOR = {'r': 235, 'g': 39, 'b': 40, 'a': 1} # red (from Neo4j, node border color)
#METADATA_COLOR = {'r': 255, 'g': 128, 'b': 0, 'a': 1} # orange
#URL_COLOR = {'r': 0, 'g': 204, 'b': 80, 'a': 1} # green
#HANDLE_COLOR = {'r': 120, 'g': 0, 'b': 220, 'a': 1} # purple


def create_random_node_selection_sample(n:int, session, suppress_output=False):
    """Creates a graph sample by using the random
    node selection sampling method.

    Parameters
    ----------
    n: int
        The desired sample size. This is the number of nodes in the sample.
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database
    suppress_output: bool
        if true running the queries will create no console output.

    Returns
    -------
    nx.Graph
        A random sample with size 'n', created by using
        random node selection sampling.
    """

    rand_p = 3.0* n/12000000 #probability to select a node in random node preselection
    # (there are about 12 million nodes in database, and factor 3.0 as safety to be sure to have enough nodes)

    query_random_nodes = """MATCH (n) WHERE rand()<{}
                            RETURN id(n) as id, labels(n) as labels ORDER BY rand() ASC LIMIT {}
                            """.format(rand_p, n)
    result_random_nodes = run_query(session, query_random_nodes, suppress_output=suppress_output)
    sample_nodes = [result_random_nodes['id'][i] for i in range(len(result_random_nodes['id']))]

    # Construct induced subgraph: (Between which nodes in the sample exist edges?)
    query_edges = """MATCH (n1)-[r]->(n2) WHERE id(n1) in {} and id(n2) in {}
                            RETURN id(n1) as start, id(n2) as end""".format(sample_nodes, sample_nodes)
    result_edges = run_query(session, query_edges, suppress_output=suppress_output)

    # Build networkx graph object from the query result:
    sample_graph = build_nxgraph_random_node(n, result_random_nodes, result_edges)

    return sample_graph


def build_nxgraph_random_node(n, result_random_nodes, result_edges):
    """ Builds and returns networkx graph from the result of the
    random node selection queries.

    The DUMMY label is being filtered out.

    Parameters
    ----------
    n: int
        The desired sample size. This is the number of nodes in the sample.
    result_random_nodes:
        result from the query "query_random_nodes"
    result_edges:
        result from the query "query_edges"

    Returns
    -------
    nx.Graph
        The networkx Graph that contains the nodes and edges from
        the query results without the DUMMY labels.
    """

    sample_edges = [[result_edges['start'][i],result_edges['end'][i]] for i in range(len(result_edges['start']))]
    sample_random_node = nx.Graph()

    for i in range(n):

        #Determine label and filtering DUMMY label out:
        if 'DUMMY' in result_random_nodes['labels'][i]:
            result_random_nodes['labels'][i].remove('DUMMY')
        node_label = result_random_nodes['labels'][i][0]

        sample_random_node.add_node(result_random_nodes['id'][i], label=node_label)

    for edge in sample_edges:
        sample_random_node.add_edge(edge[0],edge[1])

    return sample_random_node


def create_random_walk_sample(sample_size:int, session, c=0.15, suppress_output=False):
    """Returns a random walk sample of the graph database.

    Performs a random walk on the graph from the database
    and puts all visited nodes and edges in

    We use our own algorithm to have control of the exact parameters.
    The algortihm's outline is the following:

    # Goal: Sample graph with m nodes using a random walk algorithm.

    # Idea of Algorithm:
    # 1: Choose starting node s uniformly at random and add it to sample graph S
    # 2: v <- s
    # 3: if rand()<0.15 (=c): jump back to start node s (step 2)
    # 4: (v,w) = e <- choose one neighbor w of v at random  # -- Note: we will need to perform one cypher query for each realization of this step
    # 5: Add e and w to the sample graph S.
    # 6: v <- w
    # 7: Jump back to step 3 if sample graph still has < m nodes

    # keep track of number of visited nodes -> if this number is too small over a long time,
    # s might be part of a small isolated component. In that case start again with another start node s'


    Parameters
    ----------
    sample_size: int
        the number of nodes that should be contained in the
        random walk sample.
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database
    c: int
        probability for each step to jump back to the start node.
        The default value 0.15 is the one suggested in

        Jure Leskovec and Christos Faloutsos.
        “Sampling from large graphs”.
        Proceedings of the ACM SIGKDD International Conference on
        Knowledge Discovery and Data Mining 2006 (Jan. 2006),
        pp. 631–636
    suppress_output: bool
        if true running the queries will create no console output

    Returns
    -------
    nx.Graph
        A random sample with size 'n', created by using
        random walk sampling.
    """

    # 0 ----------------------------------------------------
    # - Prepare parameters and variables -

    k = 0 # counter of performed steps of the random walk
    restart_limit = sample_size*100    # If after this number of steps the sample graph does not contain m nodes,
                            # the algorithm should be restarted again with different start node
                            # (might have happened that start node was in small isolated community)


    v = 0 # current node in each step of algorithm from which we continue our random walk
    creation_finished = False # flag indicating if sample of required size has been successfully created




    while not creation_finished : # if creation was stopped because not enough nodes were found, restart form here

        sample = nx.Graph() # resulting sample, will be filled with nodes represetned by their ids
        step_count = 0 # counts numbers of steps taken during walk (number of taken edges)

        # 1 ----------------------------------------------------
        # - Select random start node -

        # note: first construct small subset of nodes with cypher and then use python's random
        # module to select 1 node from that random subset.
        # Using "LIMIT 1" would yield directly one random node. This would however not be a uniformly at random chosen
        # node, as nodes with smaller ids would be favored as they are higher in the returned list of the query

        query_random_subset_ids = """MATCH (n) where rand()<0.000001 return id(n) as id"""
        result_random_subset_ids = run_query(session, query_random_subset_ids, suppress_output=suppress_output)
        random_ids = result_random_subset_ids['id']

        #now select 1 random element from this subset:
        random_index = random.randrange(0, len(random_ids))
        start = random_ids.pop(random_index)

        sample.add_node(start)
        sample.nodes[start]['label'] = query_label(start, session)
        print("Start node {} selected. Executing random walk sampling ...".format(start))

        # 2 ----------------------------------------------------
        # - Initialize the current node to be the start node -

        while sample.number_of_nodes() < sample_size and step_count < restart_limit:

            v = start

            # 3 ----------------------------------------------------
            # - Jump back to start node with probability (1-c) -

            while random.uniform(0, 1) >= c and sample.number_of_nodes() < sample_size:

                print("Progress: Current sample contains {} nodes.".format(sample.number_of_nodes()))
                # 4 ----------------------------------------------------
                # - Select random neighbor of current node -

                query_random_neighbor = """MATCH (n)-[]-(m)  WHERE id(n) = {}
                                        WITH collect(id(m)) as Neighbors, size(collect(id(m))) as neighborCount
                                        RETURN Neighbors[toInteger(round( rand()*(neighborCount-1) ))] as id""".format(v)
                result_random_neighbor = run_query(session, query_random_neighbor, suppress_output=suppress_output)
                w = result_random_neighbor['id'][0]
                #Additionally query node label of found neighbor:
                #w_label = query_label(w, session)

                # 5 ----------------------------------------------------
                # - Add new edge to python graph -

                sample.add_edge(v,w)
                #sample.nodes[w]['label'] = w_label

                # 6 ----------------------------------------------------
                # - Update current node -

                v = w
                step_count += 1


        if sample.number_of_nodes() >= sample_size:
            creation_finished = True
            print("Sample of size ", sample_size, "successfully created.")
            print("Step count: ",step_count)

    return sample



def query_label(node_id:int, session):
    """ Returns the label of the node with id 'node_id' from database.

    Parameters
    ----------
    node_id: int
        id of a node in the database
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database


    Returns
    -------
    str
        the label of the node with id 'node_id'.
        The 'DUMMY' label will be filtered out such that only exactly one label will be returned.


    """

    query_labels = """MATCH (n) WHERE id(n)={} return labels(n) as label""".format(node_id)
    result_labels = run_query(session, query_labels, suppress_output=True)
    #filter DUMMY label out:
    if 'DUMMY' in result_labels['label'][0]:
        result_labels['label'][0].remove('DUMMY')
    label = result_labels['label'][0][0]

    return label


def query_name(node_id:int, session):
    """ Returns the name of the node with id 'node_id' from database.

    By name we mean the attribute "nodeId" in our database. It usually
    has the form [labelname][node content], e.g.
    "creatorPID Service tgpid-service-3.7.0-SNAPSHOT.201503231204"
    for a node with label "creator" and node content
    "PID Service tgpid-service-3.7.0-SNAPSHOT.201503231204"
    (the content is stored in the attribute "creator")

    Parameters
    ----------
    node_id: int
        id of a node in the database
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database


    Returns
    -------
    str
        the name (nodeId attribtute) of the node in the database
        with id 'node_id'.


    """

    query_name = """MATCH (n) WHERE id(n)={} return n.nodeId as name""".format(node_id)
    result_name = run_query(session, query_name, suppress_output=True)
    name = result_name['name'][0]

    return name


def add_label_attributes(sample, session):
    """Add label attributes for Gephi plot.

    By label we mean the node label from the Neo4j database. The name of
    the latter label attribute will be 'neo_label', such that it does not
    interfere with the 'label' attribute of the networkx that is the
    what is being displayed in gephi as the node name.

    Parameters
    ----------
    sample: nx.Graph
        A graph sample to which we want to set the 'viz' attribute to the
        predefined color values in correspondence with the neo4j labels.
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database

    Returns
    -------
    nx.Graph
        The same graph with 'neo_label' attribute for all nodes.
    """
    for v in sample.nodes():

        v_label = query_label(v, session)
        sample.nodes[v]['neo_label'] = v_label

    return sample



def add_color_attributes(sample):
    """Add color attributes for Gephi plot.


    Parameters
    ----------
    sample: nx.Graph
        A graph sample to which we want to set the 'viz' attribute to the
        predefined color values in correspondence with the labels from neo4j
        which are stored in the this graph in the node attribute 'neo_4j'.

    Returns
    -------
    nx.Graph
        The same graph with 'viz' attribute for all nodes.
    """
    for v in sample.nodes():
        if not 'neo_label' in sample.nodes[v]:
            return sample
        if sample.nodes[v]['neo_label'] == 'handle':
            sample.nodes[v]['viz'] = {'color': HANDLE_COLOR}
        elif sample.nodes[v]['neo_label'] == 'url':
            sample.nodes[v]['viz'] = {'color': URL_COLOR}
        else:
            sample.nodes[v]['viz'] = {'color': METADATA_COLOR}
    return sample



def add_name_attributes(sample, session):
    """Add name attributes for Gephi plot.

    By name we mean the attribute "nodeId" in our database. It usually
    has the form [labelname][node content], e.g.
    "creatorPID Service tgpid-service-3.7.0-SNAPSHOT.201503231204"
    for a node with label "creator" and node content
    "PID Service tgpid-service-3.7.0-SNAPSHOT.201503231204"
    (the content is stored in the attribute "creator")

    However, we only add this for metadata nodes, as the names of
    handles and URL are just long and not interesting for the plot.

    Parameters
    ----------
    sample: nx.Graph
        A graph sample to which we want to set the 'viz' attribute to the
        predefined color values in correspondence with the labels.
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database
    Returns
    -------
    nx.Graph
        The same graph with new 'label' attribute for all nodes containing
        the names (attribtue "nodeId" in Neo4j).
        The former 'label' attributes are moved to attribute 'neo_label'.
    """
    for v in sample.nodes():

        if 'neo_label' in sample.nodes[v]: # data must be labelled!

            # only add name for metadata nodes under 'label': (handles and URLs keep label "handle"/"url")
            if (not sample.nodes[v]['neo_label'] == 'handle') and (not sample.nodes[v]['neo_label'] == 'url'):
                sample.nodes[v]['label'] = query_name(v, session)
            else:
                sample.nodes[v]['label'] = ""
        else:
            print("graph is not labelled. Please call method add_label_attributes on this graph first.")
            return sample
    return sample

def add_gephi_attributes(sample, session):
    """Makes sample ready to be Gephi-plottable.

    In particular, it adds to the nodes attributes holding
    the node label names from the Neo4j database, the colors
    in accordance with these node labels and the names of the
    metadata nodes.

    Parameters
    ----------
    sample: nx.Graph
        a graph sample
    session: neo4j.Session (https://neo4j.com/docs/api/python-driver/current/api.html#session)
        The session object of the started session for querying the database
    Returns
    -------
    nx.Graph
        The provided graph sample with added node attributes.
    """

    sample = add_label_attributes(sample, session)
    sample = add_color_attributes(sample)
    sample = add_name_attributes(sample, session)

    return sample
