"""Here we define some fuctions for writing and reading statistical data to or
from JSON files, save plots to pdf files, and save and load graph samples.
"""

from pathlib import Path
import os
import json
from datetime import datetime
import glob
from os.path import join
import matplotlib.pyplot as plt
from src.config import STATISTICS_FOLDER_NAME, PLOTS_FOLDER_NAME, GRAPH_SAMPLES_FOLDER_NAME
import pandas as pd
import networkx as nx
import pickle


def save_statistics_from_dataframe(df, db_property: str):
    """Saves database statistics from a dataframe to a newly created JSON file
     in the statistics folder.

    The name of the created file has the following format:
    "stats__[`db_property`]__[timestamp].json"

    Parameters
    ----------
    df: 'pandas.core.frame.DataFrame'
        the statistical data that should be saved in JSON file
    db_property: str
        Name of the property, of which `df` contains information. This name
        will be part of the name of the created JSON file.

    """
    #data_dict = df.to_dict()
    #save_statistics_from_dict(data_dict, db_property)


    filename = construct_filename(db_property)

    df.to_json(filename, orient='columns')

    print("Data saved as {}.".format(filename))



def save_statistics_from_dict(statistics: dict, db_property: str):
    """Saves database statistics from a dictionary to a newly created JSON file
     in the statistics folder.

    The name of the created file has the following format:
    "stats__" + `db_property` + "__" + timestamp + ".json"

    Parameters
    ----------
    statistics: dictionary
        the statistical data that should be saved. The exact structure of the
        dict depends on the measured property
    db_property: str
        describes the property of which the statistics should be saved

    """

    filename = construct_filename(db_property)

    with open(filename, 'w') as file:
        json.dump(statistics, file)

    print("Data saved as {}.".format(filename))



def construct_filename(property_name: str):
    """Return a filename of the following format:

    "stats__" + `property_name` + "__" + timestamp + ".json"

    The timestamp will be set according to the system time.


    Parameters
    ----------
    property_name: str
        describes a property of teh database in words

    Returns
    -------
    str
        the filename following the format above

    """
    # create folder, if it not exists:
    Path(os.path.join(os.getcwd()), STATISTICS_FOLDER_NAME).mkdir(exist_ok=True)

    timestamp = int(datetime.now().timestamp()) # timestamp of when the data was saved
    # (it should be saved directly after running the query as it is more important to
    # know when the query was run that when it was saved)

    filename = "{}/stats__{}__{}.json".format(STATISTICS_FOLDER_NAME, property_name, timestamp)

    return filename



def find_stats_files(property_name: str):
    """Returns all names of json files containing collected statistical data
    of the graph database property specified by `property_name`.

    Parameters
    ----------
    name: str
        The name of a graph property

    Returns
    -------
    list of str
        The filenames of json files in the statistics folder matching the
        following pattern:
        "stats__" + `property_name` + "__" + timestamp + ".json"
    """
    files = [f for f in glob.glob(join(STATISTICS_FOLDER_NAME, 'stats__{}__*.json'.format(property_name)))]
    return files



def find_newest_stats_file(property_name: str):
    """Returns name of newest json file containing collected statistical data
    of the graph database property specified by `property_name` and the time of
    the database access.

    Parameters
    ----------
    name: str
        The name of a graph property

    Returns
    -------
    str
        The filename of the most recent json file in the statistics folder
        matching the following pattern:
        "stats__" + `property_name` + "__" + timestamp + ".json"

    datetime.datetime
        The time when the file was created

    """
    data_files = find_stats_files(property_name)

    if not data_files:
        print("ERROR: File loading failed because there are no statistics of the requested property. Please check your spelling of the property name.")
        return None, None

    #extract timestamps from filname strs:
    timestamps = [int(filename.split(".")[0].split("__")[-1]) for filename in data_files]
    times = [datetime.fromtimestamp(timestamps[i]) for i in range(len(data_files))]

    #find index of most recent file
    newest = times.index(max(times))

    return data_files[newest], times[newest]



def find_all_stats_files(property_name: str):
    """Returns names of all json file containing collected statistical data
    of the graph database property specified by `property_name`togehter with the
     times of the database accesses.

    Parameters
    ----------
    name: str
        The name of a graph property

    Returns
    -------
    str
        The filenames of all json files in the statistics folder
        matching the following pattern:
        "stats__" + `property_name` + "__" + timestamp + ".json"

    datetime.datetime
        The times when the stats files were created (usually immediately
         after the database access)

    """
    data_files = find_stats_files(property_name)

    if not data_files:
        print("ERROR: File loading failed because there are no statistics of the requested property. Please check your spelling of the property name.")
        return None, None

    #extract timestamps from filname strs:
    timestamps = [int(filename.split(".")[0].split("__")[-1]) for filename in data_files]
    times = [datetime.fromtimestamp(timestamps[i]) for i in range(len(data_files))]

    return data_files, times



def load_statistics(property_name: str):
    """Loads newest statistical data from a JSON-file inside the statistics
    folder.

    Parameters
    ----------
    property_name: str
        name of a property of the graph database. This should also be the name
        of a JSON file containing the desired statistics

    Returns
    -------
    dict
        The data from the json file specified by `filename`
    datetime.datetime
        The time when the statistics file was created
    """

    filename, time_created = find_newest_stats_file(property_name)
    if filename:
        with open(filename, "r") as read_file:
            data = json.load(read_file)

        return data, time_created
    else:
        return None, None



def load_all_statistics(property_name: str):
    """Loads all statistical data from a JSON-file inside the statistics folder.

    Parameters
    ----------
    property_name: str
        name of a property of the graph database. This should also be the name of
        a JSON file containing the desired statistics

    Returns
    -------
    dict
        The data from the json file specified by `filename`
    datetime.datetime
        The time when the statistics file was created
    """

    data_list = []
    time_created_list = []

    filenames, times_created = find_all_stats_files(property_name)
    for filename, time_created in zip(filenames,times_created):
        if filename:
            with open(filename, "r") as read_file:
                data = json.load(read_file)

            data_list.append(data)
            time_created_list.append(time_created)
        else:
            data_list.append(None)
            time_created_list.append(None)

    return data_list, time_created_list



def save_plot(property_name: str):
    """Saves a matplotlib plot as png file in the plots folder.

    The name of the saved image file will be of the following format:
    "plot__" + `property_name` + "__" + timestamp + ".png"

    Parameters
    ----------
    property_name: str
        The name of the property that the plots contains information of.
    """
    timestamp = int(datetime.now().timestamp()) # timestamp of when the plot was created
    filename = "{}/plot__{}__{}.pdf".format(PLOTS_FOLDER_NAME, property_name, timestamp)
    plt.savefig(filename, format="pdf", dpi=300, bbox_inches = 'tight')

    print("Plot saved as {}.".format(filename))



def save_sample(graph:nx.Graph, sampling_method:str):
    """ Saves a graph sample as both pickle file and gexf file.

    A timestamp will be created which will be part of the filename of both
    created files.

    The pickle file will contained a serialized version of the networkx graph
    that we can load again easily to further compute statistics on the sample.

    The gexf file can be used to visualize the graph with Gephi.


    Parameters
    ----------
    graph: nx.Graph
        A networkx graph object representing a graph sample.
    sampling_method: str
        The name of the used sampling method,
        e.g. random_walk or random_node_selection
    """
    # Create timestamp of when the gexf file was created to
    # 1) distinguish between different samples with same parameters
    # 2) database is changing. File should be saved directly after sample
    #    creation to now in which state the database was a time of sampling
    timestamp = int(datetime.now().timestamp())
    sample_size = graph.number_of_nodes()
    filename_prefix = "{}/sample__{}__{}__{}".format(GRAPH_SAMPLES_FOLDER_NAME, sampling_method, sample_size, timestamp)

    #save pickle:
    filename_pickle = filename_prefix + ".pickle"
    with open(filename_pickle, 'wb') as f:
        pickle.dump(graph, f)
    print("Sample graph saved as {}.".format(filename_pickle))

    #save gexf:
    filename_gexf = filename_prefix + ".gexf"
    nx.write_gexf(graph, filename_gexf)
    print("Gephi readable file saved as {}.".format(filename_gexf))



def load_samples(sampling_method:str, size:int):
    """Returns all stored graph samples with specified parameters.

    Parameters
    ----------
    name: str
        The name of a sampling method for which we want to find the created
        samples.
    size: int
        The size of the samples for which we want to find the files.

    Returns
    -------
    list of nx.Graph
        graph samples with specified sampling method and size
    """
    files = find_sample_files(sampling_method, size)
    samples = []
    for filename in files:
        with open(filename, 'rb') as f:
            sample = pickle.load(f)
            samples.append(sample)
    return samples



def find_sample_files(sampling_method:str, size:int):
    """Returns all names of pickle files containing serialized graph samples
    of the graph database sampled by the specified method.

    Parameters
    ----------
    name: str
        The name of a sampling method for which we want to find the created
        samples.
    size: int
        The size of the samples for which we want to find the files.

    Returns
    -------
    list of str
        The filenames of pickle files in the graph samples folder matching the
        following pattern:
        "sample__" + `sampling_method` + * + ".pickle"
    """
    files = [f for f in glob.glob(join(GRAPH_SAMPLES_FOLDER_NAME, 'sample__{}__{}__*.pickle'.format(sampling_method, size)))]

    return files
