"""This module provides some functions to configure matplotlib plots.
"""
import matplotlib.pyplot as plt

def settings(title: str, xlabel: str, ylabel: str, logarithmic: bool):
    """ Sets basic plotting properties.

    Parameters
    ----------
    title: str
        the title of the plot
    xlabel: str
        the label of the x axes
    ylabel: str
        the label of the y axes
    logarithmic: bool
        if True the values of the y axes will be plotted on a logarithmic scale
    """
    plt.title(title, weight='bold', fontsize=14)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if logarithmic:
        plt.gca().set_yscale("log") # logarithmic plot
