"""Here we define some paths to certain directories used in this project and the
URL to the graph database.
"""

STATISTICS_FOLDER_NAME = "statistics"
PLOTS_FOLDER_NAME = "plots"
GRAPH_SAMPLES_FOLDER_NAME = "graph_samples"

DB_URL = 'bolt+ssc://graphdb.gwdg.de:7687'
DB_NAME = 'pidgraph'
